<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookRating extends Model
{
    protected $primaryKey = ['isbn', 'reviewer_id'];
    protected $fillable = [
        'reviewer_id', 'isbn', 'rating'
    ];
    public $timestamps = false;
}
