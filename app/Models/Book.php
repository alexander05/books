<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $primaryKey = 'isbn';
    protected $fillable = [
        'isbn', 'title', 'author', 'publication', 'image_small', 'image_middle', 'image_large', 'publisher'
    ];
    public $timestamps = false;

    public function reviewers()
    {
        return $this->belongsToMany('App\Models\Reviewer', 'book_reviewer', 'book_id', 'reviewer_id')->withPivot('rating');
    }

}
