<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reviewer extends Model
{
    protected $fillable = ['location', 'age'];
    public $timestamps = false;

    public function books()
    {
        return $this->belongsToMany('App\Model\Book');
    }
}
