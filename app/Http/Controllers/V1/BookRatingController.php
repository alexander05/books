<?php

namespace App\Http\Controllers\V1;

use App\Models\Book;
use Illuminate\Http\Request;

class BookRatingController extends Controller
{
    public function read($isbn, $reviewer_id)
    {
        try {
            $rating = Book::findOrFail($isbn)
                ->reviewers()
                ->where('id', $reviewer_id)
                ->getResults()
                ->first()
                ->pivot;
        } catch (\Exception $e) {
            return response('Not found', 404);
        }
        return response()->json($rating);
    }

    public function create(Request $request, $isbn, $reviewer_id)
    {
        try {
            Book::findOrFail($isbn)
                ->reviewers()
                ->attach($reviewer_id, ['rating' => $request->get('rating') ?: 0]);
        }
        catch (\Exception $e) {
            return response('Not Acceptable', 406);
        }
        return response()->json('created');
    }

    public function update(Request $request, $isbn, $reviewer_id)
    {
        try {
            $rating = Book::findOrFail($isbn)
                ->reviewers()
                ->where('id', $reviewer_id)
                ->getResults()
                ->first();
            if ($request->get('rating')) {
                $rating->pivot->rating = $request->get('rating');
                $rating->pivot->save();
            }
        } catch (\Exception $e) {
                return response('Not found', 404);
        }
        return response()->json($rating->pivot);
    }

    public function delete($isbn, $reviewer_id)
    {
        try {
            Book::findOrFail($isbn)
                ->reviewers()
                ->detach([$reviewer_id]);
        } catch (\Exception $e) {
            return response('Not found', 404);
        }
        return response()->json('deleted');
    }
}