<?php

namespace App\Http\Controllers\V1;

use App\Models\Reviewer;
use Illuminate\Http\Request;

class ReviewerController extends Controller
{
    public function read($id)
    {
        try {
            $reviewer = Reviewer::findOrFail($id);
        } catch (\Exception $e) {
            return response('Not found', 404);
        }
        return response()->json($reviewer);
    }

    public function create(Request $request)
    {
        try {
            $reviewer = Reviewer::create($request->all());
        } catch (\Exception $e) {
            return response('Not Acceptable', 406);
        }
        return response()->json($reviewer);
    }

    public function update(Request $request, $id)
    {
        try {
            $reviewer = Reviewer::findOrFail($id);
            $reviewer->update($request->all());
        } catch (\Exception $e) {
            return response('Not found', 404);
        }
        return response()->json($reviewer);
    }

    public function delete($id)
    {
        try {
            $reviewer = Reviewer::findOrFail($id);
            $reviewer->delete();
        } catch (\Exception $e) {
            return response('Not found', 404);
        }
        return response()->json('deleted');
    }
}
