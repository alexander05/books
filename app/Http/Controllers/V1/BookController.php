<?php

namespace App\Http\Controllers\V1;

use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function read($id)
    {
        try {
            $book = Book::findOrFail($id);
        } catch (\Exception $e) {
            return response('Not found', 404);
        }
        return response()->json($book);
    }

    public function create(Request $request)
    {
        try {
            $book = Book::create($request->all());
        } catch (\Exception $e) {
            return response('Not Acceptable', 406);
        }
        return response()->json($book);
    }

    public function update(Request $request, $id)
    {
        try {
            $book = Book::findOrFail($id);
            $book->update($request->all());
        } catch (\Exception $e) {
            return response('Not found', 404);
        }
        return response()->json($book);
    }

    public function delete($id)
    {
        try {
            $book = Book::findOrFail($id);
            $book->delete();
        } catch (\Exception $e) {
            return response('Not found', 404);
        }
        return response()->json('deleted');
    }
}