<?php

namespace App\Http\Middleware;

use Closure;


class HttpBasicAuth
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->isMethod('get') &&
            ($request->getUser() != env('API_ADMIN_NAME') || $request->getPassword() != env('API_ADMIN_PASS'))) {

            $headers = array('WWW-Authenticate' => 'Basic');
            return response('Unauthorized.', 401, $headers);
        }
        return $next($request);
    }

}
