<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['prefix' => 'v1', 'middleware' => 'basic_auth'], function ($group) {
    $group->get('/reviewer/{id}', 'V1\ReviewerController@read');
    $group->post('/reviewer/{id}', 'V1\ReviewerController@update');
    $group->put('/reviewer', 'V1\ReviewerController@create');
    $group->delete('/reviewer/{id}', 'V1\ReviewerController@delete');

    $group->get('/book/{id}', 'V1\BookController@read');
    $group->post('/book/{id}', 'V1\BookController@update');
    $group->put('/book', 'V1\BookController@create');
    $group->delete('/book/{id}', 'V1\BookController@delete');

    $group->get('/book/rating/{isbn}_{reviewer_id}', 'V1\BookRatingController@read');
    $group->post('/book/rating/{isbn}_{reviewer_id}', 'V1\BookRatingController@update');
    $group->put('/book/rating/{isbn}_{reviewer_id}', 'V1\BookRatingController@create');
    $group->delete('/book/rating/{isbn}_{reviewer_id}', 'V1\BookRatingController@delete');
});

