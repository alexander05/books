### Проект на Lumen PHP Framework ###

* Установка не требуется, лишь в vhosts нужно цепляться к папке /public, загрузить дамп из репозитория https://bitbucket.org/alexander05/boooks.dump и проверить настройки подключения базы данных в файле env, в корне проекта
* Пользователь с полными правами - admin - admin, используется basic авторизация 
* Минимальная версия PHP 5.6

### Роутинг ###

### /v1/reviewer/{id} GET чтение ###
* Content-Type: application/json
* авторизация не обязательна

### /v1/reviewer/{id} POST изменение ###
* Content-Type: application/json
* Authorization: Basic YWRtaW46YWRtaW4=
* Data: {"field1": "{field1}", "field2": "{field2}"}

### /v1/reviewer/{id} DELETE удаление ###
* Content-Type: application/json
* Authorization: Basic YWRtaW46YWRtaW4=

### /v1/reviewer PUT создание ###
* Content-Type: application/json
* Authorization: Basic YWRtaW46YWRtaW4=
* Data: {"field1": "{field1}", "field2": "{field2}"}

### /v1/book/{isbn} GET чтение ###
* Content-Type: application/json
* авторизация не обязательна

### /v1/book/{isbn} POST изменение ###
* Content-Type: application/json
* Authorization: Basic YWRtaW46YWRtaW4=
* Data: {"field1": "{field1}", "field2": "{field2}"}

### /v1/book/{isbn} DELETE удаление ###
* Content-Type: application/json
* Authorization: Basic YWRtaW46YWRtaW4=

### /v1/book PUT создание ###
* Content-Type: application/json
* Authorization: Basic YWRtaW46YWRtaW4=
* Data: {"field1": "{field1}", "field2": "{field2}"}


### /v1/book/rating/{isbn}_{reviewer_id} GET чтение ###
* Content-Type: application/json
* авторизация не обязательна

### /v1/book/rating/{isbn}_{reviewer_id} POST изменение ###
* Content-Type: application/json
* Authorization: Basic YWRtaW46YWRtaW4=
* Data: {"rating": "{value}"}

### /v1/book/rating/{isbn}_{reviewer_id} DELETE удаление ###
* Content-Type: application/json
* Authorization: Basic YWRtaW46YWRtaW4=

### /v1/book/rating/{isbn}_{reviewer_id} PUT создание ###
* Content-Type: application/json
* Authorization: Basic YWRtaW46YWRtaW4=
* Data: {"rating": "{value}"}